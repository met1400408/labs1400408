package lab02;

public class CoffeeMachine {

    private int waterLevel;
    private int beanLevel;
    private int cleanStatus;
    private int powerOn;

    public void coffeeMachine(int waterLevel, int beanLevel, int cleanStatus, int powerOn) {
        this.waterLevel = 100;
        this.beanLevel = 100;
        this.cleanStatus = 10;
        this.powerOn = 0;
    }

    public void fillWater() {
        if (waterLevel < 100) {
            this.waterLevel = 100;
            System.out.println("Water tank is filled.");
        } else if (waterLevel == 100) {
            System.out.println("Water tank is already full.");
        }
    }

    public void fillBeans() {
        if (beanLevel < 100) {
            this.beanLevel = 100;
            System.out.println("Bean tank is filled.");
        } else if (beanLevel == 100) {
            System.out.println("Bean tank is already full.");
        }
    }

    public void pressOnOff() {
        if (this.powerOn == 0) {
            this.powerOn = 1;
            System.out.println("Rinsing...");
            this.waterLevel += -20;
            System.out.println("Rinsing finished, machine ready to use.");
        } else if (this.powerOn == 1) {
            this.powerOn = 0;
            System.out.println("Rinsing...");
            this.waterLevel += -20;
            System.out.println("Rinsing finished, machine shutting down.");

        }
    }

    public void espresso() {
        if (this.powerOn == 0) {
            System.out.println("(machine is unresponsive)");
        } else if (powerOn == 1 && this.waterLevel >= 1 && this.beanLevel >= 1 && this.cleanStatus >= 1) {
            this.waterLevel += -30;
            this.beanLevel += -8;
            this.cleanStatus += -1;
            System.out.println("Espresso is coming out.");
        } else if (this.cleanStatus < 1) {
            System.out.println("Machine needs to be cleaned." + "\n" + "Espresso is coming out.");
        } else if (this.waterLevel < 1) {
            System.out.println("Not enough water.");
        } else if (this.beanLevel < 1) {
            System.out.println("Not enough beans.");

        }

    }

    public void coffee() {
        if (this.powerOn == 0) {
            System.out.println("(machine is unresponsive)");
        } else if (powerOn == 1 && this.waterLevel >= 1 && this.beanLevel >= 1 && this.cleanStatus >= 1) {
            this.waterLevel += -15;
            this.beanLevel += -10;
            this.cleanStatus += -1;
            System.out.println("Coffee is coming out.");
        } else if (this.cleanStatus < 1) {
            System.out.println("Machine needs to be cleaned." + "\n" + "Coffee is coming out.");
        } else if (this.waterLevel < 1) {
            System.out.println("Not enough water.");
        } else if (this.beanLevel < 1) {
            System.out.println("Not enough beans.");
        }
    }

    public void clean() {
        if (this.powerOn == 0) {
            System.out.println("(machine is unresponsive)");
        } else if (this.powerOn == 1) {
            System.out.println("Cleaning...");
        }
        this.cleanStatus += +10;
        this.waterLevel += -50;
        System.out.println("Cleaning finished, machine ready to use.");
    }
}
