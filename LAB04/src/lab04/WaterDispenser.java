
package lab04;
import java.io.IOException;
import java.util.Scanner;
public class WaterDispenser implements Runnable{

//muuttujat
    private int waterLevel;
    private boolean valveClosed;
    private int amount;
    private Thread waterRunning;

    
    //konstruktori
    public void waterDispenser(int waterLevel, boolean valve) {
        this.waterLevel = 100;
        this.valveClosed = true;
        this.amount = 0;
    }
    
    //metodit
    
    public void setAmount(int amount) {
        this.amount = amount;
    }
    
    public void dispenseWater() {
        if (this.valveClosed == true) {
            System.out.println("Open the valve first!");
        } else if (this.valveClosed == false) {
            System.out.println("How much would you like to have water?");
            Scanner reader = new Scanner(System.in);
            this.amount = reader.nextInt();
            if (this.amount > waterLevel) {
                System.out.println("You cannot have more water than you have.");
            } else {
                System.out.println("Water coming");
                printWater();
            }
        }
        this.waterRunning = new Thread(this);
        this.waterRunning.start();
        
    }

    public void printWater() {
        for (int i = 0; i < amount; i++) {
            try {
                Thread.sleep(1000);//1000 milliseconds is one second.
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("*");
            waterLevel = waterLevel - 1;
            if (valveClosed == true) {
                break;
            }
        }
    }


    public void valveControl() {
         if (this.valveClosed == true) {
            this.valveClosed = false;
            System.out.println("Valve is open!");
        } else if (this.valveClosed == false) {
            this.valveClosed = true;
            System.out.println("Valve is closed!");
        } else {
            System.out.println("ERROR");
        }
    }

    @Override
    public void run() {

        valveControl();
         dispenseWater();
         printWater();
        while (!this.valveClosed && this.amount != 0) {
            if (this.waterLevel >= 1) {
                this.waterLevel += -1;
                this.amount += -1;
                System.out.print("* ");
                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (this.amount == 0) {
                    System.out.println("All amount you want is out");
                    break;
                }
            } else {
                System.out.println("Not enough water!");
                break;
            }
        }
    }
}
