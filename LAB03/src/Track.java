import java.util.ArrayList;
public class Track{

    String trackName;
    String artistName;
    String trackLength;
    
    
    //CONSTRUCTOR
    public Track(String trackName, String artistName, String trackLength){
        this.trackName=trackName;
        this.artistName=artistName;
        this.trackLength=trackLength;
    }
    
    
    public void playTrack(){
        System.out.println("Playing track:" + trackName);
    }
    
    

    


}