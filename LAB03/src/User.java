import java.util.ArrayList;
import java.util.Scanner;

public class User {

        public static void main (String args[]){ 
        Scanner reader = new Scanner(System.in);
        Cd cd1=new Cd();
        
        System.out.println("Sony Walkman");
        System.out.println("press any key to continue press q to quit.");
        
        while (true) {
        String command=reader.nextLine();
        if (command.equals("q")) {
        break;
        }
        
        System.out.println("press a to add songs.");
        System.out.println("press n to show number of songs");
        System.out.println("press s to show song.");
        System.out.println("press r to remove song.");
        System.out.println("press c to check song");
        System.out.println("press p to play songs.");
        System.out.println("press f to play next song.");
        System.out.println("press b to play previous song.");
        System.out.println("press q to quit.");
        System.out.println("WRITE A COMMAND:");
       
       if (command.equals("a")){
           cd1.addSongs();
       }
       else if(command.equals("n")){
           cd1.numberOfSongs();
       }
       else if(command.equals("s")){
           cd1.showSong();
       }
       else if(command.equals("r")){
           cd1.removeSong();
       }
       else if(command.equals("c")){
           cd1.checkSong();
       }
       else if(command.equals("p")){
           cd1.playSongs();
       }
       else if(command.equals("f")){
           cd1.nextSong();
       }
       else if(command.equals("b")){
           cd1.previousSong();
       }
    }
 }
}